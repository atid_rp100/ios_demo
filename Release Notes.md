## RELEASE HISTORY

##### _Release date: 2022-01-12_

| Name   | Description |
| ------ | ------ |
| Application Name | RFID_Demo_20220101.ipa |
| Demo Version | 202201.01 |
| Supported F/W Version | ut-5.1.1.13.4 and above |

## What's New

 - [ ] 6B and Rail Tag Inventory Feature added
 <details>
     <summary markdown="span" >Screenshots</summary>
      <div align="left">
        <img width="24%" src="https://gitlab.com/atid-share/asset/-/raw/main/ios_tagtype_00.png"  alt="Setting screen" title="Settings"</img>
        <b> >> </b>
        <img width="24%" src="https://gitlab.com/atid-share/asset/-/raw/main/ios_tagtype_01.png" alt="List screen" title="Setting Lists"></img>
        <b> >> </b>
        <img width="24%" src="https://gitlab.com/atid-share/asset/-/raw/main/ios_tagtype_02.png" alt="dialog " title="Option dialog"></img>
        <img width="24%" src="https://gitlab.com/atid-share/asset/-/raw/main/ios_tagtype_03.png"  alt="Setting screen" title="Settings"</img>
        <b> >> </b>
        <img width="24%" src="https://gitlab.com/atid-share/asset/-/raw/main/ios_tagtype_04.png" alt="List screen" title="Setting Lists"></img>
       </div>
      </details>


## Support
 For SDK and other inquiries please contact inquiry@atid1.com
